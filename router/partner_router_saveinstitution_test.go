package router_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"testing"

	"bitbucket.org/ifirlana/pintek-api/request"

	"bitbucket.org/ifirlana/pintek-api/router"
	"github.com/gin-gonic/gin"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/stretchr/testify/assert"
)

func getPOSTInstitutionSavePayloadJson() []byte {
	req := request.ReqInstitutiondata{
		PartnerID:                    "153",
		InstitutionCode:              "def",
		InstitutionName:              "abc",
		InstitutionType:              1,
		StatusActive:                 1,
		InstitutionAccreditation:     "A",
		InstitutionLevel:             []string{"SD"},
		InstitutionBranchName:        "ghi",
		IsBranchHq:                   1,
		InstitutionBranchAddress:     "jkl",
		InstitutionBranchProvince:    "DKI Jakarta",
		InstitutionBranchCity:        "Jakarta Selatan",
		InstitutionBranchDistrict:    "Pasar Minggu",
		InstitutionBranchVillage:     "Pasar Minggu",
		InstitutionBranchStatus:      1,
		InstitutionBankAccountName:   "PT Institution",
		InstitutionBankName:          "BCA",
		InstitutionBankAccountNumber: "123213123123123",
		PartnerName:                  "abcd",
	}

	res, _ := json.Marshal(req)
	return res
}

func TestSaveInstitutionSuccess(t *testing.T) {
	db := LoadDB()

	routerPartner := router.Partner{}
	routerPartner = routerPartner.New(db)
	// Build our expected body
	body := gin.H{
		"success": true,
	}
	// Grab our router
	router := router.SetupRouter(routerPartner)

	// Perform a POST request with that handler.
	payload := getPOSTInstitutionSavePayloadJson()
	w := performRequest(router, "POST", "/partner-153/institution/save", bytes.NewBuffer(payload))
	fmt.Println(w.Body.String())
	// Assert we encoded correctly,
	// the request gives a 200
	assert.Equal(t, http.StatusOK, w.Code)
	// Convert the JSON response to a map
	var response map[string]interface{}
	err := json.Unmarshal([]byte(w.Body.String()), &response)
	// Grab the value & whether or not it exists
	value, exists := response["success"]
	// Make some assertions on the correctness of the response.
	assert.Nil(t, err)
	assert.True(t, exists)
	assert.Equal(t, body["success"], value)

}
