package lib_test

import (
	"bytes"
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/jlaffaye/ftp"
)

// TestUpload File inside
func TestConnectionFTP(t *testing.T) {
	path := fmt.Sprintf("%s:%s", os.Getenv("FTP_ADDR"), os.Getenv("FTP_PORT"))
	c, err := ftp.Dial(path, ftp.DialWithTimeout(5*time.Second))
	if err != nil {
		t.Errorf("error: %s", err.Error())
		return
	}

	err = c.Login(os.Getenv("FTP_USER"), os.Getenv("FTP_PASS"))
	if err != nil {
		t.Errorf("error: %s", err.Error())
		return
	}
	if err := c.Quit(); err != nil {
		t.Errorf("error: %s", err.Error())
	}
}

// TestUpload File inside
func TestUploadFile(t *testing.T) {
	path := fmt.Sprintf("%s:%s", os.Getenv("FTP_ADDR"), os.Getenv("FTP_PORT"))
	c, err := ftp.Dial(path, ftp.DialWithTimeout(5*time.Second))
	if err != nil {
		t.Errorf("error: %s", err.Error())
		return
	}

	err = c.Login(os.Getenv("FTP_USER"), os.Getenv("FTP_PASS"))
	if err != nil {
		t.Errorf("error: %s", err.Error())
		return
	}

	store_path := fmt.Sprintf("%s/partner/test-file.txt", os.Getenv("FTP_STORE_PATH"))

	data := bytes.NewBufferString("Hello World")

	err = c.Stor(store_path, data)
	if err != nil {
		t.Errorf("error: %s", err.Error())
		return
	}

	err = c.Login(os.Getenv("FTP_USER"), os.Getenv("FTP_PASS"))
	if err != nil {
		t.Errorf("error: %s", err.Error())
		return
	}
	if err := c.Quit(); err != nil {
		t.Errorf("error: %s", err.Error())
	}
}
