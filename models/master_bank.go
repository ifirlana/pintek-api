package models

// MasterBank from master_bank
type MasterBank struct {
	ID uint `gorm:"primary_key"`
	Name string `gorm:"column:name"`
}
