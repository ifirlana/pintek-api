package request

// ErrorParameter should be equal
const ErrorParameter = "invalid parameter"

// ErrorDatabase database parameter
const ErrorDatabase = "error database"

// ErrorNotFound result
const ErrorNotFound = "error result not found"

// ErrorDataAlreadyExist result if duplicate state
const ErrorDataAlreadyExist = "error data already exist"
