package models

// MPartnerTokenLeads for table m_partner_token_leads
type MPartnerTokenLeads struct {
	ID              uint   `gorm:"primary_key"`
	MPartnerID      uint   `gorm:"column:m_partner_id"`
	Token           string `gorm:"column:token"`
	StudentNumber   string `gorm:"column:student_number"`
	StudentDob      string `gorm:"column:student_dob"`
	InstitutionCode string `gorm:"column:institution_code"`
	InstitutionName string `gorm:"column:institution_name"`
	Active          int    `gorm:"column:active"`
}
