package router

import "github.com/gin-gonic/gin"

// SetupRouter use as collection router
func SetupRouter(partner Partner) *gin.Engine {

	r := gin.Default()
	r.Use(JSONMiddleware())

	r.POST("/:partner_slug/leads/incoming", partner.RouterIncomingLeads)
	r.POST("/:partner_slug/institution/save", partner.RouterSaveInstitution)

	return r
}

// JSONMiddleware middleware to return json
func JSONMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Content-Type", "application/json")
		c.Next()
	}
}
