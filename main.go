package main

import (
	"fmt"
	"os"

	"bitbucket.org/ifirlana/pintek-api/router"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {
	conentionName := fmt.Sprintf("%s:%s@(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", os.Getenv("DB_USERNAME"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_HOST"), os.Getenv("DB_PORT"), os.Getenv("DB_NAME"))

	db, err := gorm.Open("mysql", conentionName)
	if err != nil {
		fmt.Println(err.Error())
		panic("failed to connect database")
	}

	partner := router.Partner{}

	partner = partner.New(db)

	r := router.SetupRouter(partner)
	r.Run(os.Getenv("RUN_PORT"))
}
