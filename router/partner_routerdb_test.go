package router_test

import (
	"fmt"
	"testing"

	"bitbucket.org/ifirlana/pintek-api/models"
	"bitbucket.org/ifirlana/pintek-api/router"
)

func TestDbSuccess(t *testing.T) {
	db := LoadDB()

	partnerinst := router.Partner{}

	partnerinst = partnerinst.New(db)
	partner := models.MPartner{}

	if partnerinst.Db.Raw("select mp.* from m_partner mp join m_partner_credential mps on mp.partner_id = mps.partner_id where mp.is_active = ? and mp.partner_id = ? and mps.token = ? and mps.slug = ?", 1, "153", "abcdef", "partner-153").Scan(&partner).Error != nil {
		t.Fatal("error")
	}
	fmt.Println(partner)
}
