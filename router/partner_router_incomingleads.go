package router

import (
	"fmt"
	"net/http"
	"os"

	"gopkg.in/go-playground/validator.v9"

	"bitbucket.org/ifirlana/pintek-api/lib"
	"bitbucket.org/ifirlana/pintek-api/models"
	"bitbucket.org/ifirlana/pintek-api/request"
	"github.com/gin-gonic/gin"
)

// RouterIncomingLeads starting Here
func (partnerInstance *Partner) RouterIncomingLeads(c *gin.Context) {
	incRequest := request.ReqIncomingleads{}

	err := c.BindJSON(&incRequest)
	if err != nil {
		fmt.Printf("%s \n", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": request.ErrorParameter,
		})
		return
	}

	validate := validator.New()
	if err = validate.Struct(incRequest); err != nil {
		fmt.Printf("%s \n", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": request.ErrorParameter,
		})
		return
	}

	// TODO: save request to database

	partnerSlug := c.Param("partner_slug")
	tokenHeader := c.Request.Header.Get("token")
	if partnerSlug == "" || tokenHeader == "" {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": request.ErrorParameter,
		})
		return
	}

	partner := models.MPartner{}

	dbResult := partnerInstance.Db.Raw("select mp.* from m_partner mp join m_partner_credential mps on mp.partner_id = mps.partner_id where mp.is_active = 1 and mp.partner_name = ? and mp.partner_id = ? and mps.token = ? and mps.slug = ?", incRequest.PartnerName, incRequest.PartnerID, tokenHeader, partnerSlug).Scan(&partner)
	if dbResult.Error != nil {
		fmt.Printf("%s \n", dbResult.Error.Error())
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": request.ErrorDatabase,
		})
		return
	}

	// TODO: Review logic HERE
	// institution := models.Institution{}

	// dbResult = partnerInstance.Db.Raw("select * from institution where institution_code= ? and name = ?", incRequest.InstitutionCode, incRequest.InstitutionName).Scan(&institution)
	// if dbResult.Error != nil {
	// 	fmt.Printf("%s \n", dbResult.Error.Error())
	// 	c.JSON(http.StatusBadRequest, gin.H{
	// 		"status":  http.StatusBadRequest,
	// 		"success": false,
	// 		"message": request.ErrorDatabase,
	// 	})
	// 	return
	// }

	// student := models.Student{
	// 	NimNisn:  incRequest.StudentNumber,
	// 	BornDate: incRequest.StudentDob,
	// }
	// dbResult = partnerInstance.Db.Table("student").Create(&student)
	// if dbResult.Error != nil {
	// 	fmt.Printf("%s \n", dbResult.Error.Error())
	// 	c.JSON(http.StatusBadRequest, gin.H{
	// 		"status":  http.StatusBadRequest,
	// 		"success": false,
	// 		"message": request.ErrorDatabase,
	// 	})
	// 	return
	// }
	tokenLeadsOld := models.MPartnerTokenLeads{}
	dbResult = partnerInstance.Db.Raw("select * from m_partner_token_leads where student_dob = ? and student_number = ? and institution_code = ? and institution_name = ? and m_partner_id = ? and active = 1", incRequest.StudentDob, incRequest.StudentNumber, incRequest.InstitutionCode, incRequest.InstitutionName, partner.ID).Scan(&tokenLeadsOld)
	fmt.Printf("TOKEN old: %s and %s \n", tokenLeadsOld.Token, fmt.Sprintf("%v", tokenLeadsOld.ID))
	if tokenLeadsOld.ID != 0 {
		dbResult = partnerInstance.Db.Exec("UPDATE m_partner_token_leads SET active = 0 WHERE student_dob = ? and student_number = ? and institution_code = ? and institution_name = ? and m_partner_id = ?", incRequest.StudentDob, incRequest.StudentNumber, incRequest.InstitutionCode, incRequest.InstitutionName, partner.ID)
		if dbResult.Error != nil {
			fmt.Printf("Cannot update: %s \n", err.Error())
			c.JSON(http.StatusInternalServerError, gin.H{
				"status":  http.StatusBadRequest,
				"success": false,
				"message": err.Error(),
			})
			return
		}
	}

	tokenLib := lib.Token{}
	token, err := tokenLib.CreateRandom(partner.PartnerID)
	if err != nil {
		fmt.Printf("%s \n", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": err.Error(),
		})
		return
	}

	incomingLead := models.MPartnerTokenLeads{
		Token:           token,
		StudentDob:      incRequest.StudentDob,
		StudentNumber:   incRequest.StudentNumber,
		InstitutionCode: incRequest.InstitutionCode,
		InstitutionName: incRequest.InstitutionName,
		Active:          1,
		MPartnerID:      partner.ID,
	}
	dbResult = partnerInstance.Db.Table("m_partner_token_leads").Create(&incomingLead)
	if dbResult.Error != nil {
		fmt.Printf("%s \n", dbResult.Error.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": dbResult.Error.Error(),
		})
		return
	}

	link := fmt.Sprintf("%s?token=%s", os.Getenv("LINK_PINTEK_LEAD"), token)

	c.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"success": true,
		"link":    link,
	})
	return
}
