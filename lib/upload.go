package lib

import (
	"fmt"
	"mime/multipart"
	"os"
	"path/filepath"
	"time"

	"github.com/jlaffaye/ftp"
)

// Upload to FTP Pintek
func Upload(file *multipart.FileHeader) (fullpath string, err error) {
	filename := filepath.Base(file.Filename)
	path := fmt.Sprintf("%s:%s", os.Getenv("FTP_ADDR"), os.Getenv("FTP_PORT"))

	c, err := ftp.Dial(path, ftp.DialWithTimeout(5*time.Second))
	if err != nil {
		return
	}

	err = c.Login(os.Getenv("FTP_USER"), os.Getenv("FTP_PASS"))
	if err != nil {
		return
	}

	storePath := fmt.Sprintf("%s/partner/%v-%s", os.Getenv("FTP_STORE_PATH"), time.Now().UnixNano(), filename)

	fullpath = fmt.Sprintf("%s/%s", os.Getenv("FILESERVER_URL"), storePath)

	out, err := os.Open(file.Filename)
	if err != nil {
		return
	}

	err = c.Stor(storePath, out)
	if err != nil {
		return
	}

	err = c.Login(os.Getenv("FTP_USER"), os.Getenv("FTP_PASS"))
	if err != nil {
		return
	}
	if err = c.Quit(); err != nil {
		return
	}
	return
}
