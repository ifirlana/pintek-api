package router

import (
	"fmt"
	"net/http"
	"strings"

	"bitbucket.org/ifirlana/pintek-api/lib"

	"bitbucket.org/ifirlana/pintek-api/models"

	"bitbucket.org/ifirlana/pintek-api/request"
	"gopkg.in/go-playground/validator.v9"

	"github.com/gin-gonic/gin"
)

// RouterSaveInstitution starting Here
func (partnerInstance *Partner) RouterSaveInstitution(c *gin.Context) {
	incRequest := request.ReqInstitutiondata{}

	err := c.BindJSON(&incRequest)
	if err != nil {
		fmt.Printf("%s \n", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": request.ErrorParameter,
		})
		return
	}

	validate := validator.New()
	if err = validate.Struct(incRequest); err != nil {
		fmt.Printf("%s \n", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": request.ErrorParameter,
		})
		return
	}

	partnerSlug := c.Param("partner_slug")
	tokenHeader := c.Request.Header.Get("token")
	if partnerSlug == "" || tokenHeader == "" {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": request.ErrorParameter,
		})
		return
	}

	// Check partner exist
	partner := models.MPartner{}

	dbResult := partnerInstance.Db.Raw("select mp.* from m_partner mp join m_partner_credential mps on mp.partner_id = mps.partner_id where mp.is_active = 1 and mp.partner_name = ? and mp.partner_id = ? and mps.token = ? and mps.slug = ?", incRequest.PartnerName, incRequest.PartnerID, tokenHeader, partnerSlug).Scan(&partner)
	if dbResult.Error != nil {
		fmt.Printf("partner_not_found: %s \n", dbResult.Error.Error())
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": request.ErrorDatabase,
		})
		return
	}

	// Load Master_Province
	province := models.MasterProvince{}

	dbResult = partnerInstance.Db.Raw("select * from master_province where village = ? and sub_district = ? and district = ? and province = ?", incRequest.InstitutionBranchVillage, incRequest.InstitutionBranchDistrict, incRequest.InstitutionBranchCity, incRequest.InstitutionBranchProvince).Scan(&province)
	if dbResult.Error != nil {
		fmt.Printf("master_province: %s \n", dbResult.Error.Error())
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": request.ErrorNotFound,
		})
		return
	}

	// Load Master Bank
	bank := models.MasterBank{}

	dbResult = partnerInstance.Db.Raw("select * from master_bank where name = ?", incRequest.InstitutionBankName).Scan(&bank)
	if dbResult.Error != nil {
		fmt.Printf("master_bank: %s \n", dbResult.Error.Error())
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": request.ErrorNotFound,
		})
		return
	}

	// check institution_code
	institution := models.Institution{}

	dbResult = partnerInstance.Db.Raw("select * from institution where institution_code = ?", incRequest.InstitutionCode).Scan(&institution)
	if dbResult.Error == nil {
		fmt.Printf("institution already exist \n")
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": request.ErrorDataAlreadyExist,
		})
		return
	}

	// If its has file upload then upload to FTP

	pathLogo := ""

	file, _ := c.FormFile("institution_logo")
	if file != nil {
		pathLogo, err = lib.Upload(file)
		if err != nil {
			fmt.Printf("upload_error : %s \n", err.Error())
			c.JSON(http.StatusInternalServerError, gin.H{
				"status":  http.StatusInternalServerError,
				"success": false,
				"message": err.Error(),
			})
			return
		}
	}

	statusActive := ""
	if incRequest.StatusActive == 1 {
		statusActive = "1"
	} else if incRequest.StatusActive == 2 {
		statusActive = "0"
	}

	statusBranchActive := ""
	if incRequest.InstitutionBranchStatus == 1 {
		statusBranchActive = "1"
	} else if incRequest.InstitutionBranchStatus == 2 {
		statusBranchActive = "2"
	}

	// save to institution
	institution = models.Institution{
		InstitutionCode:  incRequest.InstitutionCode,
		InstitutionCatID: 2,
		Name:             incRequest.InstitutionName,
		LogoPath:         pathLogo,
		WebsiteURL:       incRequest.InstitutionWebsite,
		Accreditation:    strings.ToLower(incRequest.InstitutionAccreditation),
		Level:            strings.Join(incRequest.InstitutionLevel, ","),
		Status:           fmt.Sprintf("%v", incRequest.InstitutionStatus),
		Active:           statusActive,
	}

	dbResult = partnerInstance.Db.Table("institution").Create(&institution)
	if dbResult.Error != nil {
		fmt.Printf("institution: %s \n", dbResult.Error.Error())
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": request.ErrorDatabase,
		})
		return
	}

	// save to institution_branch
	institutionBranch := models.InstitutionBranches{
		InstitutionID: institution.ID,
		Name:          incRequest.InstitutionBranchName,
		IsHq:          fmt.Sprintf("%v", incRequest.IsBranchHq),
		ProvinceID:    province.ID,
		PicName:       incRequest.InstitutionPicName,
		PicEmail:      incRequest.InstitutionPicEmail,
		PicPhone:      incRequest.InstitutionPicPhone,
		Address:       incRequest.InstitutionBranchAddress,
		Active:        statusBranchActive,
	}

	dbResult = partnerInstance.Db.Table("institution_branches").Create(&institutionBranch)
	if dbResult.Error != nil {
		fmt.Printf("institution_branches: %s \n", dbResult.Error.Error())
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": request.ErrorDatabase,
		})
		return
	}

	// save to institution_bank_acc
	institutionBankAccount := models.InstitutionBankAcc{
		NameOnBank:          incRequest.InstitutionBankAccountName,
		BankID:              bank.ID,
		InstitutionBranchID: institutionBranch.ID,
		InstitutionID:       institution.ID,
		AccNumber:           incRequest.InstitutionBankAccountNumber,
	}

	dbResult = partnerInstance.Db.Table("institution_bank_acc").Create(&institutionBankAccount)
	if dbResult.Error != nil {
		fmt.Printf("institution_bank_acc: %s \n", dbResult.Error.Error())
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": request.ErrorDatabase,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status":             http.StatusOK,
		"success":            true,
		"institution_number": institution.ID,
		"institution_name":   incRequest.InstitutionName,
	})
	return
}
