CREATE TABLE `m_partner` (
 `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
 `partner_type_id` int(11) unsigned DEFAULT NULL,
 `partner_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
 `partner_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
 `is_active` tinyint(1) DEFAULT NULL,
 `join_date` date DEFAULT NULL,
 `terminate_date` date DEFAULT NULL,
 `created_datetime` datetime DEFAULT CURRENT_TIMESTAMP,
 `updated_at` datetime DEFAULT NULL,
 PRIMARY KEY (`id`),
 KEY `fk_partner_type` (`partner_type_id`),
 CONSTRAINT `fk_partner_type` FOREIGN KEY (`partner_type_id`) REFERENCES `m_partner_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
)

CREATE TABLE `m_partner_credential` (
 `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
 `partner_id` int(11) unsigned DEFAULT NULL,
 `token` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
 `slug` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
 `created_datetime` datetime DEFAULT CURRENT_TIMESTAMP,
 `updated_datetime` datetime DEFAULT NULL,
 PRIMARY KEY (`id`)
)
CREATE TABLE `m_partner_type` (
 `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
 `code` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
 `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
 PRIMARY KEY (`id`)
)

CREATE TABLE `m_partner_token_leads` (
    `id` BIGINT(20) unsigned NOT NULL AUTO_INCREMENT,
    `m_partner_id` int(11) unsigned DEFAULT NULL,
    `token` VARCHAR(200) DEFAULT NULL,
    `student_number` VARCHAR(200) DEFAULT NULL,
    `student_dob` VARCHAR(200) DEFAULT NULL,
    `institution_code` VARCHAR(200) DEFAULT NULL,
    `institution_name` VARCHAR(200) DEFAULT NULL,
    `active` tinyint(1) DEFAULT NULL,
    `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
    `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
     PRIMARY KEY (`id`),
     CONSTRAINT `fk_partner_token_leads` FOREIGN KEY (`m_partner_id`) REFERENCES `m_partner` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
)

ALTER TABLE `institution` ADD COLUMN `institution_code` VARCHAR(100) DEFAULT NULL AFTER `institution_cat_id`;
ALTER TABLE `institution` ADD COLUMN `partner_id` VARCHAR(50) DEFAULT NULL AFTER `institution_code`;
