include .env
export
lib-test:
	go test -v -count 1 ./lib

test:
	go test -v ./...

router-test:
	go test -v -count 1 ./router/partner_router_test.go
	go test -v -count 1 ./router/partner_routerdb_test.go

model-test:
	go test -v -count 1 ./models

main:
	go run main.go
