package request

// ReqInstitutiondata struct of response
type ReqInstitutiondata struct {
	PartnerID                    string      `json:"partner_id" validate:"required,alphanum"`
	PartnerName                  string      `json:"partner_name" validate:"required,alphanum"`
	InstitutionCode              string      `json:"institution_code" validate:"required"`
	InstitutionName              string      `json:"institution_name" validate:"required"`
	InstitutionType              int         `json:"institution_type" validate:"required,oneof=1"` //1 = Formal or 2 = Non Formal
	InstitutionLogo              interface{} `json:"institution_logo,omitempty"`                   // it will be File poc??
	InstitutionWebsite           string      `json:"institution_website,omitempty"`
	StatusActive                 int         `json:"status_active" validate:"required,oneof=1 2"` // 1 = Aktif 2. or Tidak Aktif
	InstitutionAccreditation     string      `json:"institution_accreditation" validate:"required,oneof=A B C"`
	InstitutionLevel             []string    `json:"institution_level" validate:"required,gte=1"`
	InstitutionStatus            int         `json:"institution_status" validate:"required,oneof=1 2"` // 1 = Negeri or 2. Swasta
	InstitutionBranchName        string      `json:"institution_branch_name" validate:"required"`
	IsBranchHq                   int         `json:"is_branch_hq" validate:"required,oneof=1 2"`
	InstitutionBranchAddress     string      `json:"institution_branch_address" validate:"required"`
	InstitutionBranchProvince    string      `json:"institution_branch_province" validate:"required"`
	InstitutionBranchCity        string      `json:"institution_branch_city" validate:"required"`
	InstitutionBranchDistrict    string      `json:"institution_branch_district,omitempty"`
	InstitutionBranchVillage     string      `json:"institution_branch_village,omitempty"`
	InstitutionPicName           string      `json:"institution_pic_name,omitempty"`
	InstitutionPicEmail          string      `json:"institution_pic_email,omitempty" validate:"email"`
	InstitutionPicPhone          string      `json:"institution_pic_phone,omitempty"`
	InstitutionBranchStatus      int         `json:"institution_branch_status" validate:"required,oneof=1 2"`
	InstitutionBankAccountName   string      `json:"institution_bank_account_name" validate:"required"`
	InstitutionBankName          string      `json:"institution_bank_name" validate:"required"`
	InstitutionBankAccountNumber string      `json:"institution_bank_account_number" validate:"required"`
}
