package router_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"testing"

	"bitbucket.org/ifirlana/pintek-api/request"
	"bitbucket.org/ifirlana/pintek-api/router"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/stretchr/testify/assert"
)

func LoadDB() *gorm.DB {
	connectionName := fmt.Sprintf("%s:%s@(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", os.Getenv("DB_USERNAME"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_HOST"), os.Getenv("DB_PORT"), os.Getenv("DB_NAME"))
	db, err := gorm.Open("mysql", connectionName)
	if err != nil {
		fmt.Println(err.Error())
		panic("failed to connect database")
	}
	return db
}

func getPOSTPayload() string {
	params := url.Values{}
	params.Add("partner_id", "1")
	params.Add("partner_name", "2")
	params.Add("institution_code", "abc")
	params.Add("institution_name", "defg")
	params.Add("student_number", "10101010")
	params.Add("student_dob", "2020-01-11")

	return params.Encode()
}

func getPOSTPayloadJson() []byte {
	req := request.ReqIncomingleads{
		PartnerID:       "153",
		InstitutionName: "abc",
		PartnerName:     "abcd",
		InstitutionCode: "def",
		StudentDob:      "2020-01-01",
		StudentNumber:   "xxxxx",
	}

	res, _ := json.Marshal(req)
	return res
}

func performRequest(r http.Handler, method, path string, params io.Reader) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, path, params)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}

func TestIncomingLeadsSuccess(t *testing.T) {
	db := LoadDB()

	routerPartner := router.Partner{}
	routerPartner = routerPartner.New(db)
	// Build our expected body
	body := gin.H{
		"success": true,
	}
	// Grab our router
	router := router.SetupRouter(routerPartner)

	// Perform a POST request with that handler.
	payload := getPOSTPayloadJson()
	w := performRequest(router, "POST", "/partner-153/leads/incoming", bytes.NewBuffer(payload))
	fmt.Println(w.Body.String())
	// Assert we encoded correctly,
	// the request gives a 200
	assert.Equal(t, http.StatusOK, w.Code)
	// Convert the JSON response to a map
	var response map[string]interface{}
	err := json.Unmarshal([]byte(w.Body.String()), &response)
	// Grab the value & whether or not it exists
	value, exists := response["success"]
	// Make some assertions on the correctness of the response.
	assert.Nil(t, err)
	assert.True(t, exists)
	assert.Equal(t, body["success"], value)

}
