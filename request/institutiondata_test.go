package request_test

import (
	"testing"

	"bitbucket.org/ifirlana/pintek-api/request"
	"gopkg.in/go-playground/validator.v9"
)

func TestInsitutionDataValidatorRequiredWithoutLogoSuccess(t *testing.T) {
	validate := validator.New()
	institutionStruct := request.ReqInstitutiondata{
		PartnerID:                    "123",
		PartnerName:                  "xxx",
		InstitutionCode:              "xxx",
		InstitutionName:              "xx",
		InstitutionType:              1,
		InstitutionLogo:              "xx",
		InstitutionWebsite:           "xx",
		StatusActive:                 1,
		InstitutionAccreditation:     "A",
		InstitutionLevel:             []string{"SD"},
		InstitutionStatus:            1,
		InstitutionBranchName:        "xx",
		IsBranchHq:                   1,
		InstitutionBranchAddress:     "xx",
		InstitutionBranchProvince:    "xx",
		InstitutionBranchCity:        "xx",
		InstitutionBranchDistrict:    "xx",
		InstitutionBranchVillage:     "xx",
		InstitutionPicName:           "xx",
		InstitutionPicEmail:          "a@a.com",
		InstitutionPicPhone:          "xx",
		InstitutionBranchStatus:      1,
		InstitutionBankAccountName:   "xx",
		InstitutionBankName:          "xx",
		InstitutionBankAccountNumber: "xx",
	}
	if err := validate.Struct(institutionStruct); err != nil {
		t.Errorf("\n %s \n", err.Error())
	}
}
