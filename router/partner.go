package router

import (
	"github.com/jinzhu/gorm"
)

// Partner Context
type Partner struct {
	Db *gorm.DB
}

// New initiate object
func (p Partner) New(db *gorm.DB) Partner {
	p.Db = db
	return p
}
