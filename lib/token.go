package lib

import (
	"crypto/sha1"
	"fmt"
	"time"
)

// DateFormat make default formatted
const DateFormat = "2006-01-02 15:04:05"

// TimeZone Format
const TimeZone = "Asia/Bangkok"

// ExpiredDay is constanta for expired day
const ExpiredDay = 1

// Token library
type Token struct {
	Value   string
	Created string
	Expired string
	Status  bool
}

// CreateRandom generate token string with random format
func (t Token) CreateRandom(key string) (token string, err error) {
	h := sha1.New()

	loc, err := time.LoadLocation(TimeZone)
	if err != nil {
		return
	}

	dt := time.Now().In(loc).Format(DateFormat)
	mergetwo := key + dt
	h.Write([]byte(mergetwo))

	token = fmt.Sprintf("%x", h.Sum(nil))
	return
}

// Generate process generate
func (t Token) Generate() (token Token, err error) {
	loc, err := time.LoadLocation(TimeZone)
	if err != nil {
		return
	}
	token.Value, err = t.CreateRandom("")
	if err != nil {
		return
	}
	token.Created = time.Now().In(loc).Format(DateFormat)
	token.Expired = time.Now().In(loc).AddDate(0, 0, ExpiredDay).Format(DateFormat)
	token.Status = true
	return
}
