package request

// ReqIncomingleads struct of request
type ReqIncomingleads struct {
	PartnerID       string `json:"partner_id" validate:"required"`
	PartnerName     string `json:"partner_name" validate:"required,alphanum"`
	InstitutionCode string `json:"institution_code" validate:"required"`
	InstitutionName string `json:"institution_name" validate:"required"`
	StudentNumber   string `json:"student_number" validate:"required"`
	StudentDob      string `json:"student_dob" validate:"required"`
}
