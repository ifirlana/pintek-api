package models

// Institution from table institution
type Institution struct {
	ID               uint   `gorm:"primary_key"`
	InstitutionCode  string `gorm:"column:institution_code"`
	Name             string `gorm:"column:name"`
	Accreditation    string `gorm:"column:accreditation"`
	InstitutionCatID uint   `gorm:"column:institution_cat_id"`
	LogoPath         string `gorm:"column:logo_path"`
	WebsiteURL       string `gorm:"column:website_url"`
	Active           string `gorm:"column:active"`
	Level            string `gorm:"column:level"`
	Status           string `gorm:"column:status"`
}

// InstitutionBranches from table institution_branches
type InstitutionBranches struct {
	ID            uint   `gorm:"primary_key"`
	InstitutionID uint   `gorm:"column:institution_id"`
	Name          string `gorm:"column:name"`
	IsHq          string `gorm:"column:is_hq"`
	ProvinceID    uint   `gorm:"column:province_id"`
	PicName       string `gorm:"column:pic"`
	PicEmail      string `gorm:"column:pic_email"`
	PicPhone      string `gorm:"column:phone"`
	Address       string `gorm:"column:address"`
	Active        string `gorm:"column:active"`
}

// InstitutionBankAcc from
type InstitutionBankAcc struct {
	ID                  uint   `gorm:"primary_key"`
	InstitutionID       uint   `gorm:"column:institution_id"`
	InstitutionBranchID uint   `gorm:"column:institution_branch_id"`
	BankID              uint   `gorm:"column:bank_id"`
	NameOnBank          string `gorm:"column:name_on_bank"`
	AccNumber           string `gorm:"column:acc_number"`
}
