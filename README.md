# README #

This project using go version 1.14
Libraries are using from:
- ginzhu
- GinGonic

There are two endpoints:
- `/<partner_name>/leads/incoming`
- `/<partner_name>/institution/save`

## Getting Started
cp `.envexample` into `.env`. set as your environment used.

### Environment
```
DB_HOST=
DB_PORT=
DB_USERNAME=
DB_PASSWORD=
DB_NAME=
```

and then, for development currently using `MAKE` by using command line

`make main`

and For Test Driven Development
use command line

`make test`

Enjoy.