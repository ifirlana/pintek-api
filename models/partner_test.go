package models_test

import (
	"fmt"
	"os"
	"testing"

	"bitbucket.org/ifirlana/pintek-api/models"
	"github.com/jinzhu/gorm"

	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func load() *gorm.DB {
	connectionName := fmt.Sprintf("%s:%s@(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", os.Getenv("DB_USERNAME"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_HOST"), os.Getenv("DB_PORT"), os.Getenv("DB_NAME"))
	db, err := gorm.Open("mysql", connectionName)
	if err != nil {
		fmt.Println(err.Error())
		panic("failed to connect database")
	}
	return db
}

func TestQueryModel(t *testing.T) {
	db := load()

	partner := models.MPartner{}
	if db.Table("m_partner").Where("partner_id = ?", "153").First(&partner).Error != nil {
		fmt.Println(db.Error.Error())
		t.Errorf("fail load : %s", db.Error.Error())
		return
	}

	fmt.Println(partner)
}

func TestFindPartner(t *testing.T) {
	db := load()

	partner := models.MPartner{}
	if db.Raw("select mp.* from m_partner mp join m_partner_credential mps on mp.partner_id = mps.partner_id where mp.is_active = ? and mp.partner_id = ? and mps.token = ? and mps.slug = ?", 1, "153", "abcdef", "partner-153").Scan(&partner).Error != nil {
		fmt.Println(db.Error.Error())
		t.Errorf("fail load : %s", db.Error.Error())
		return
	}

	fmt.Println(partner)
}

func TestCreatewithModel(t *testing.T) {
	// db := load()
	// partner := models.MPartner{
	// 	PartnerName:   "bean and carrot",
	// 	PartnerID:     "153",
	// 	PartnerTypeID: 1,
	// }
	// if db.Table("m_partner").Create(&partner).Error != nil {
	// 	fmt.Println(db.Error.Error())
	// 	t.Errorf("fail load : %s", db.Error.Error())
	// 	return
	// }
}
