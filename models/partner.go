package models

// MPartner list of partner
type MPartner struct {
	ID            uint   `gorm:"primary_key"`
	PartnerTypeID uint   `gorm:"column:partner_type_id"`
	PartnerID     string `gorm:"column:partner_id"`
	PartnerName   string `gorm:"column:partner_name"`
	IsActive      uint   `gorm:"column:is_active"`
	JoinDate      string `gorm:"column:join_date"`
	TerminateDate string `gorm:"column:terminate_date"`
}
