package request_test

import (
	"encoding/json"
	"testing"

	"bitbucket.org/ifirlana/pintek-api/request"
	"gopkg.in/go-playground/validator.v9"
)

func TestIncomingLeadsValidatorSuccess(t *testing.T) {
	validate := validator.New()

	reqStruct := request.ReqIncomingleads{
		PartnerID:       "1",
		PartnerName:     "oke",
		InstitutionCode: "oke partner",
		InstitutionName: "inst name",
		StudentNumber:   "asdasdasdas13123123",
		StudentDob:      "2020-10-12",
	}

	if err := validate.Struct(reqStruct); err != nil {
		t.Errorf("\n %s \n", err.Error())
	}
}

func TestIncomingLeadsWithJsonValidatorSuccess(t *testing.T) {
	validate := validator.New()
	reqStruct := request.ReqIncomingleads{}

	requestParam := `{"partner_id": "susan", "partner_name": "abc", "institution_code": "asd", "institution_name": "qwe", "student_number": "asdasdasd", "student_dob": "2020-01-01"}`
	json.Unmarshal([]byte(requestParam), &reqStruct)

	if err := validate.Struct(&reqStruct); err != nil {
		t.Errorf("\n %s \n", err.Error())
	}
}
