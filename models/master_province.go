package models

// MasterProvince from master_bank
type MasterProvince struct {
	ID          uint   `gorm:"primary_key"`
	Village     string `gorm:"column:village"`
	SubDistrict string `gorm:"column:sub_district"`
	District    string `gorm:"column:district"`
	Province    string `gorm:"column:province"`
}
