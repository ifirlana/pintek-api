package lib_test

import (
	"errors"
	"fmt"
	"testing"
	"time"

	"bitbucket.org/ifirlana/pintek-api/lib"
)

func TestRandomToken(t *testing.T) {
	token := lib.Token{}
	result, _ := token.CreateRandom("abc")
	fmt.Println(result)
	t.Log(result)
	if result == "" {
		t.Errorf("Empty token field")
	}
}

func TestGenerateToken(t *testing.T) {
	token := lib.Token{}
	result, _ := token.Generate()
	loc, _ := time.LoadLocation(lib.TimeZone)

	fmt.Println(result.Created)
	if result.Created != time.Now().In(loc).Format(lib.DateFormat) {
		t.Errorf("%s \n", errors.New("Create time not same"))
	}

	fmt.Println(result.Expired)
	if result.Expired != time.Now().AddDate(0, 0, lib.ExpiredDay).In(loc).Format(lib.DateFormat) {
		t.Errorf("%s \n", errors.New("Expired time not same"))
	}

	fmt.Println(result.Value)
	if result.Value == "" {
		t.Errorf("%s \n", errors.New("Token Not Generated"))
	}

	fmt.Println(result.Status)
	if result.Status == false {
		t.Errorf("%s \n", errors.New("Status token not true"))
	}
}
